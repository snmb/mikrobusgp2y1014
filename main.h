#include <18F25K20.h>
#device ADC=10

#use delay(crystal=20000000)

#use rs232(baud=115200, parity=N, xmit=PIN_C6, rcv=PIN_C7, bits=8, stream = DUIstream, ERRORS)
//#use i2c(master,sda=PIN_C4, scl=PIN_C3, force_sw)
#use rtos(timer=0, minor_cycle=1 ms)

#define SNGP2Y1014_3v3

#define LED1  PIN_A1
#define LED2  PIN_A2
#define LED3  PIN_A3

#define AN     PIN_A0
#define RST    PIN_B3
#define CS     PIN_B1
#define SCK    PIN_C3
#define MISO   PIN_C5
#define MOSI   PIN_C4

#define PWM    PIN_C2
#define pINT   PIN_B0
#define RX     PIN_C7
#define TX     PIN_C6
#define SCL    PIN_C3
#define SDA    PIN_C4
