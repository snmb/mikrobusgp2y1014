#include "main.h"
#include "dui.c"
#include "SNGP2Y1014.c"

//TASK MESSAGE CHECK
#task(rate = 100 ms, max = 300 us)
void task_SNGP2Y1014check();

int8 task_SNGP2Y1014state = 0;

//====================================
#task(rate = 20 ms, max = 300 us)
void task_DUIcheck();

void formInit(void);
void formRead(unsigned int8 channel, unsigned int8* data);

//-------------------------------------------------
void task_SNGP2Y1014check() {
   task_SNGP2Y1014state ++;
   switch (task_SNGP2Y1014state) {
   case 1: SNGP2Y1014_check(); break;
   case 2: 
      DUISendStateTrueFalse(0, SNGP2Y1014_isValid);
      if (!SNGP2Y1014_isValid) task_SNGP2Y1014state = 0;
      break;
   case 3:
      DUISendStateTrueFalse(1, SNGP2Y1014_isSmoke);
      DUISendInt16(2, SNGP2Y1014_raw);
      DUISendInt16(3, SNGP2Y1014_raw_filtered);
      DUISendInt16(4, SNGP2Y1014_getDust_filtred());
      break;
   default: task_SNGP2Y1014state = 0;
   }
   
}

void task_DUIcheck() {
   DUICheck();
}

void formInit(void) {   
   DUIAddLed(0, 0, (char*)"valid");
   DUIAddLed(1, 0, (char*)"smoke");
   DUIAddLabel(2, (char*)"ADC raw");
   DUIAddLabel(3, (char*)"ADC filter");
   DUIAddLabel(4, (char*)"Dust, ug/m3");

}

void formRead(unsigned int8 channel, unsigned int8* data) {

}

//implementation
void initialization() {
    SET_TRIS_A( 0x00 );
    SET_TRIS_B( 0x01 );
    SET_TRIS_C( 0x80 );
    DUIInit();
    DUIOnFormInit = formInit;
    DUIOnReadData = formRead;
    output_low(PWM);
    output_low(PIN_A1);
    output_low(PIN_A2);
    output_low(PIN_A3);
    SNGP2Y1014_init(PIN_B0, sAN0, 0, V3ADC10);
}

void main() {
   initialization();
   rtos_run();
}
