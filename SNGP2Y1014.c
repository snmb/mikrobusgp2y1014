//************************************************
//  Dust Sensors module GP2Y1014
//  Tumanov Stanislav Alexandrovich 
//  12/11/2021
//************************************************

#define SNGP2Y1014_array_size 20

enum SNGP2Y1014_PADCtype {V3ADC10, V5ADC10, V3ADC12, V5ADC12};

//public
int1 SNGP2Y1014_isValid = 0;
int1 SNGP2Y1014_isSmoke = 0;
void SNGP2Y1014_init(int32 led, int32 adc, int8 channel, SNGP2Y1014_PADCtype type);
void SNGP2Y1014_check();
unsigned int16 SNGP2Y1014_getDust();
unsigned int16 SNGP2Y1014_getDust_filtred();

//private
unsigned int16 SNGP2Y1014_raw = 0;
float SNGP2Y1014_raw_filtered = 130;
float SNGP2Y1014_dust_ugm3 = 0;
float SNGP2Y1014_dust_step = 0;
float SNGP2Y1014_dust_validLimit = 0;
float SNGP2Y1014_dust_smokeLimit = 0;

int32 SNGP2Y1014_ledPin = PIN_B0;
int32 SNGP2Y1014_adcPin = sAN0;
int8 SNGP2Y1014_adcChannel = 0;
int SNGP2Y1014_adcType = V5ADC12;
unsigned int8 SNGP2Y1014_device_valid = 0;
unsigned int8 SNGP2Y1014_smoke = 0;
void SNGP2Y1014_add(float vol);
unsigned int16 SNGP2Y1014_getDust_raw(float raw) ;
void SNGP2Y1014_setValid(int1 valid);
void SNGP2Y1014_setSmoke(int1 smoke);


//=====================================================================
void SNGP2Y1014_init(int32 led, int32 adc, int8 channel, SNGP2Y1014_PADCtype type) {
  SNGP2Y1014_ledPin = led;
  output_drive(SNGP2Y1014_ledPin);
  output_high(SNGP2Y1014_ledPin); 
  SNGP2Y1014_adcPin = adc; 
  SNGP2Y1014_adcChannel = channel;
  SNGP2Y1014_adcType = type; 

  setup_adc_ports(SNGP2Y1014_adcPin, VSS_VDD);
  setup_adc(ADC_CLOCK_DIV_2|ADC_TAD_MUL_0);

  switch (SNGP2Y1014_adcType) {
    case 0: 
      SNGP2Y1014_raw_filtered = 130; 
      SNGP2Y1014_dust_step = 60;
      SNGP2Y1014_dust_validLimit = 100;
      SNGP2Y1014_dust_smokeLimit = 780;
      break;
    case 1: 
      SNGP2Y1014_raw_filtered = 85; 
      SNGP2Y1014_dust_step = 40;
      SNGP2Y1014_dust_validLimit = 70;
      SNGP2Y1014_dust_smokeLimit = 520;
      break;
    case 2: 
      SNGP2Y1014_raw_filtered = 530; 
      SNGP2Y1014_dust_step = 240;
      SNGP2Y1014_dust_validLimit = 500;
      SNGP2Y1014_dust_smokeLimit = 3150;
      break;
    default: 
      SNGP2Y1014_raw_filtered = 350; 
      SNGP2Y1014_dust_step = 160;
      SNGP2Y1014_dust_validLimit = 300;
      SNGP2Y1014_dust_smokeLimit = 2080;
      break;
  }
}

void SNGP2Y1014_check() {
  set_adc_channel(SNGP2Y1014_adcChannel);
  output_low(SNGP2Y1014_ledPin);
  delay_us(280);
  read_adc(adc_start_only);
  delay_us(10);
  SNGP2Y1014_raw = read_adc(adc_read_only);
  SNGP2Y1014_add(SNGP2Y1014_raw);
  delay_us(30);
  output_high(SNGP2Y1014_ledPin);
}

unsigned int16 SNGP2Y1014_getDust() {
  return SNGP2Y1014_getDust_raw(SNGP2Y1014_raw);
}

unsigned int16 SNGP2Y1014_getDust_filtred() {
  return SNGP2Y1014_getDust_raw(SNGP2Y1014_raw_filtered);
}

void SNGP2Y1014_add(float vol) {
  SNGP2Y1014_setValid(vol > SNGP2Y1014_dust_validLimit);
  SNGP2Y1014_setSmoke(vol > SNGP2Y1014_dust_smokeLimit); 
  if (!SNGP2Y1014_isValid) {SNGP2Y1014_raw_filtered = 0; return;}
  if (vol < SNGP2Y1014_dust_validLimit) return;
  float k = 0.01;
  float p = vol - SNGP2Y1014_raw_filtered;
  if (p > SNGP2Y1014_dust_step || p < -SNGP2Y1014_dust_step) k = 0.1;
  SNGP2Y1014_raw_filtered += p * k;
 
}

unsigned int16 SNGP2Y1014_getDust_raw(float raw) {
  switch(SNGP2Y1014_adcType) {
    case 0: SNGP2Y1014_dust_ugm3 = raw * 0.78 - 102.9; break;
    case 1: SNGP2Y1014_dust_ugm3 = raw * 1.182 - 102.8; break;
    case 2: SNGP2Y1014_dust_ugm3 = raw * 0.1951 - 103.5; break;
    default: SNGP2Y1014_dust_ugm3 = raw * 0.2957 - 103.7; break;  
  }
  if (SNGP2Y1014_dust_ugm3 < 0) SNGP2Y1014_dust_ugm3 = 0;
  return SNGP2Y1014_dust_ugm3;
}

void SNGP2Y1014_setValid(int1 valid) {
  if (valid) {
    if (SNGP2Y1014_device_valid < 4) SNGP2Y1014_device_valid ++;
    if (SNGP2Y1014_device_valid >= 4) SNGP2Y1014_isValid = true;
  } else {
    if (SNGP2Y1014_device_valid > 0) SNGP2Y1014_device_valid --;
    if (SNGP2Y1014_device_valid == 0) SNGP2Y1014_isValid = false;
  }
}

void SNGP2Y1014_setSmoke(int1 smoke) {
  if (smoke) {
    if (SNGP2Y1014_smoke < 6) SNGP2Y1014_smoke ++;
    if (SNGP2Y1014_smoke >= 6) SNGP2Y1014_isSmoke = true;
  } else {
    if (SNGP2Y1014_smoke > 0) SNGP2Y1014_smoke --;
    if (SNGP2Y1014_smoke == 0) SNGP2Y1014_isSmoke = false;
  }
}
